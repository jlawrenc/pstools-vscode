import * as vscode from "vscode";

import { CatCodingPanel } from "../panels/cat/extension/CatCodingPanel";
// import { Checker } from "../panels/check/extension/Checker";

import { TableEditor } from "../panels/l1-editor/tableEditor";

export function activate(context: vscode.ExtensionContext): void {
  context.subscriptions.push(
    vscode.commands.registerCommand("catCoding.start", () => {
      CatCodingPanel.createOrShow(context.extensionPath);
    }),
  );

  context.subscriptions.push(
    vscode.commands.registerCommand("catCoding.doRefactor", () => {
      if (CatCodingPanel.currentPanel) {
        CatCodingPanel.currentPanel.doRefactor();
      }
    }),
  );

  vscode.window.registerWebviewPanelSerializer(CatCodingPanel.viewType, {
    async deserializeWebviewPanel(webviewPanel: vscode.WebviewPanel, _state: Record<string, string>) {
      CatCodingPanel.revive(webviewPanel, context.extensionPath);
    },
  });

  // context.subscriptions.push(
  //   vscode.commands.registerCommand("pstools.opencheckspanel", () => {
  //     Checker.createOrShow(context.extensionPath);
  //   }),
  // );

  // vscode.window.registerWebviewPanelSerializer(Checker.viewType, {
  //   async deserializeWebviewPanel(webviewPanel: vscode.WebviewPanel, state: Record<string, string>) {
  //     console.log(`Got state: ${state}`);
  //     Checker.revive(webviewPanel, context.extensionPath);
  //   },
  // });

  TableEditor.activate(context);
}
