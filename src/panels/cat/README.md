# Example coding cat VSCode panel

This is derived from https://github.com/microsoft/vscode-extension-samples/tree/master/webview-sample

It will:
- load two cat commands (available via F1), coding cat and cat refactor
- define a panel that shows a different cat, based on what editor column it is in
- regularly throw an exception that a bug has been found
- change the counter when the refactor command is given

Code callable by the main extension is kept in the `extension` folder, while
code running inside the panel is kept in the `scripts` folder.