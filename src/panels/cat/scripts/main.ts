interface VSCode {
  postMessage(message: Record<string, string>): void;
  getState(): Record<string, unknown>;
  setState(state: Record<string, unknown>): void;
}

declare function acquireVsCodeApi(): VSCode;

const vscode = acquireVsCodeApi();

const oldState = vscode.getState();

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const counter = document.getElementById("lines-of-code-counter")!;
let currentCount = Number(oldState?.count || 0);
counter.textContent = "" + currentCount;

setInterval(() => {
  counter.textContent = "" + currentCount++;

  // Update state
  vscode.setState({ count: currentCount });

  // Alert the extension when the cat introduces a bug
  if (Math.random() < Math.min(0.001 * currentCount, 0.05)) {
    // Send a message back to the extension
    vscode.postMessage({
      command: "alert",
      text: "🐛  on line " + currentCount,
    });
  }
}, 100);

// Handle messages sent from the extension to the webview
window.addEventListener("message", event => {
  const message = event.data; // The json data that the extension sent
  switch (message.command) {
    case "refactor":
      currentCount = Math.ceil(currentCount * 0.5);
      counter.textContent = "" + currentCount;
      break;
  }
});
