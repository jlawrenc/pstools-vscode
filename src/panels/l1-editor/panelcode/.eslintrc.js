// eslint-disable-next-line @typescript-eslint/no-var-requires
const commonrules = require("../../../../.eslintrc.commonrules");

module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/vue3-essential",
    "@vue/typescript/recommended",
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: Object.assign({}, commonrules),
};
