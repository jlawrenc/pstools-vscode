import { WebviewPanel } from "vscode";
import { Message, MessageTypesMap } from "../../../types/messages";

export function postMessage<T extends keyof MessageTypesMap>(
  panel: WebviewPanel,
  message: Message<T>,
): Thenable<boolean> {
  return panel.webview.postMessage(message);
}
