import { join } from "path";
import {
  CancellationToken,
  Disposable,
  ExtensionContext,
  TextDocument,
  WebviewPanel,
  window,
  workspace,
  WorkspaceEdit,
  Range,
  Webview,
  Uri,
} from "vscode";

import { getNonce } from "../../extension/getNonce";
import { postMessage } from "./lib/postMessage";
import { Message, MessageTypesMap } from "../../types/messages";
import { siblingUri } from "./lib/siblingUri";

export class TableEditor {
  public static readonly viewType = "cms.l1.prescaleEditor";

  public static activate(context: ExtensionContext): Disposable {
    return window.registerCustomEditorProvider(this.viewType, new this(context));
  }

  public static getScriptsRoot(extensionRoot: string): string {
    return join(extensionRoot, "out", "panels", "l1-editor", "panelcode", "build");
  }

  constructor(private readonly context: ExtensionContext) {}

  public async resolveCustomTextEditor(
    document: TextDocument,
    panel: WebviewPanel,
    _token: CancellationToken,
  ): Promise<void> {
    panel.webview.options = {
      enableScripts: true,
    };

    panel.webview.html = this.render(panel.webview);

    // // sends an update event to the webpanel so it can update it's interface
    // const notifyUpdate = async () => {
    //   postMessage(panel, await getFilePayload(document));
    // };

    const disposable = workspace.onDidChangeTextDocument(e => {
      // TODO ? re-introduce file checks, for own file name and any constraints in schema
      // if (e.document.uri === document.uri) {
      const p = e.document.uri.path;
      const name = p.endsWith("/columns.json")
        ? "columns"
        : e.document.uri === document.uri
        ? "l1-prescales"
        : undefined;
      if (name) {
        postMessage(panel, {
          type: "file-changed",
          payload: {
            name,
            fileContent: e.document.getText(),
          },
        });
      }
      // }
    });

    panel.onDidDispose(() => disposable.dispose());

    panel.webview.onDidReceiveMessage(async <T extends keyof MessageTypesMap>(s: string) => {
      const m: Message<T> = JSON.parse(s);

      if (m.type === "file-changed") {
        // ts does not entirely reliably detect that m is now Message<"file-changed">
        const p = (m as Message<"file-changed">).payload;
        // no change (echo from our own event)
        if (!panel.visible || document.getText() === p.fileContent) {
          return;
        }
        this.updateEntireDocument(document, p.fileContent);
      } else if (m.type === "ready") {
        const columnsUri = siblingUri(document.uri, "columns");
        const c = await workspace.openTextDocument(columnsUri);
        postMessage(panel, { type: "file-changed", payload: { name: "columns", fileContent: c.getText() } });
        postMessage(panel, {
          type: "file-changed",
          payload: { name: "l1-prescales", fileContent: document.getText() },
        });
      } else {
        console.error("invalid message type", m);
      }
    });
  }

  private updateEntireDocument(document: TextDocument, content: string) {
    const edit = new WorkspaceEdit();
    edit.replace(document.uri, new Range(0, 0, document.lineCount, 0), content);
    return workspace.applyEdit(edit);
  }

  private render(webview: Webview): string {
    const root = webview.asWebviewUri(Uri.file(TableEditor.getScriptsRoot(this.context.extensionPath)));
    const nonce = getNonce();
    return `<!DOCTYPE html>
<html lang=en>
  <head>
    <meta charset=utf-8>
    <meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src ${webview.cspSource}; style-src ${webview.cspSource}; script-src 'nonce-${nonce}';">
    <link nonce="${nonce}" href=${root}/css/app.css rel=preload as=style>
    <link nonce="${nonce}" href=${root}/js/app.js rel=preload as=script>
    <link nonce="${nonce}" href=${root}/js/chunk-vendors.js rel=preload as=script>
    <link nonce="${nonce}" href=${root}/css/app.css rel=stylesheet>
  </head>
  <body>
    <div id=app>
      <p>yo</p>
      <noscript><strong>We're sorry but this doesn't work properly without JavaScript enabled. Please enable it to continue.</strong></noscript>
      <script nonce="${nonce}" src=${root}/js/chunk-vendors.js></script>
      <script nonce="${nonce}" src=${root}/js/app.js></script>
    </div>
  </body>
</html>`;
  }
}
