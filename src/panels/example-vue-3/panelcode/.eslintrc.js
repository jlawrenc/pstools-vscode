module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/vue3-essential",
    "@vue/typescript/recommended",
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    "no-console": [
      process.env.NODE_ENV === "production" ? "error" : "off",
      { allow: ["warn", "error"] }
    ],
    "no-debugger": [process.env.NODE_ENV === "production" ? "error" : "warn"],
    "@typescript-eslint/no-var-requires": 0, // interferes with js files (webpack config, e2e tests)
    "@typescript-eslint/no-use-before-define": [
      "error",
      { functions: false, classes: true, typedefs: false }
    ]
  }
};
