export interface ColumnsDocument {
  columns: Column[];
}

export interface Column {
  name: string;
}
