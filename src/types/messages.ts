export type EmptyMessage = Record<string, never>;

export interface MessageTypesMap {
  "file-changed": FileChangedMessage;
  ready: EmptyMessage;
}

export interface FileChangedMessage {
  name: "l1-prescales" | "columns";
  fileContent: string;
}

export interface Message<T extends keyof MessageTypesMap> {
  type: T;
  payload: MessageTypesMap[T];
}

export const makeMessage = <T extends keyof MessageTypesMap>(type: T, payload: MessageTypesMap[T]): Message<T> => ({
  type,
  payload,
});
