SHELL:=/bin/bash
.DEFAULT_GOAL := extension

#
# main targets
#

.PHONY: lint
lint: node_modules
	npx eslint . --fix $${CI:+--no-fix} $${CI:+--max-warnings=0} --ext ts --ext js
	npx prettier -c '**/*.ts' --write

.PHONY: extension
extension: node_modules lint panels
	npx tsc -p ./
	cp -r src/schemas out

.PHONY: package
package: # npm script will run 'make'
	npm install
	npx vsce package

.PHONY: clean
clean: panels_clean
	rm -rf out

#
# dependencies
#

node_modules: package.json
	npm install
	# npm install doesn't modify mtime
	touch node_modules

# this breaks up-to-date checks, useful for sub-makefiles
.FORCE:

# root directories for panel code
# note that in-panel code are essensially heavily sandboxed standalone 
# web apps + vscode messaging api for talking with the actual extension code
all_panels := $(shell find src/panels -type d -name panelcode)

.PHONY: panels
panels: $(addsuffix /build, $(addprefix out/, ${all_panels:src/%=%}))
out/panels/%/panelcode/build: src/panels/%/panelcode/build
	mkdir -p $@
	cp -r $< $(dir $@)
src/panels/%/panelcode/build: .FORCE
	make -C $(dir $@)

.PHONY: panels_clean
panels_clean: $(addsuffix /clean, ${all_panels})
src/panels/%/panelcode/clean:
	make -C $(dir $@) clean
