FROM cern/c8-base:20200701-2.x86_64

ENV CODESERVER_VERSION=3.5.0

RUN curl -OL https://github.com/cdr/code-server/releases/download/v${CODESERVER_VERSION}/code-server-${CODESERVER_VERSION}-amd64.rpm && \
    dnf install -y git ./code-server-${CODESERVER_VERSION}-amd64.rpm && \
    rm -rf ./code-server-${CODESERVER_VERSION}-amd64.rpm && \
    dnf clean all && \
    groupadd -r pstools && useradd -r -g pstools pstools && \
    mkdir /git /.config /.local && chown pstools:pstools /git /.config /.local && chmod 0777 /git /.config /.local && \
    mkdir -p /home/pstools && chown pstools:pstools /home/pstools

COPY docker/config.yaml /home/pstools/.config/code-server/config.yaml
# openshift starts docker images with random PIDs, $HOME=/
COPY docker/config.yaml /.config/code-server/config.yaml
COPY docker/start.sh /start.sh
COPY pstools-vscode-0.0.1.vsix /

USER pstools
WORKDIR /git

RUN code-server --install-extension /pstools-vscode-0.0.1.vsix

ENTRYPOINT ["/start.sh"]
EXPOSE 8000