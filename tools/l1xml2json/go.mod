module l1xml2json

go 1.15

require (
	github.com/google/martian v2.1.0+incompatible
	github.com/sirupsen/logrus v1.7.0
)
