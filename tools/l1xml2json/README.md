# L1XML2JSON

This application turns a prescale XML like found in the configuration database into a JSON used by the pstools-vscode project.

Example usage from source:
```bash
go run ./cmd example.xml
```
